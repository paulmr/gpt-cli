CRYSTAL_EXE=crystal

CRYSTAL_ARGS+=-p

MAIN_FILE=src/gptcli.cr
SRC_FILES=$(wildcard src/*.cr src/cmd/*.cr)

bin/gpt : $(SRC_FILES)
	mkdir -p $(dir $@)
	$(CRYSTAL_EXE) build -o $@ $(CRYSTAL_ARGS) $(MAIN_FILE)
