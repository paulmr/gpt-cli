module GPTCli
  CFG_FNAME = Path.home / Path[".config/gpt/gpt.cfg"]
  class Config
    getter api_key = nil, verbose = false, lang : String? = nil, cmd : String = "transcribe"

    def initialize
      @parser = OptionParser.new do |opts|
        opts.banner = "Usage: gpt [global options] command [command options]"
        opts.on("-k API_KEY",
                "--api-key=API_KEY",
                "GPT api key to override config") { |key| @api_key = key }
        opts.on("-l LANG", "--lang=LANG", "Language to use") { |l| @lang = l }
        opts.on("-v", "--verbose", "Verbose mode") { @verbose = true }
        opts.on("-h", "--help", "Show help") do
          puts usage
          exit
        end
        opts.on("transcribe", "transcribe audio file to text") { @cmd = "transcribe" }
        opts.on("define", "define a word") { @cmd = "define" }
        opts.on("complete", "free completion of text by GPT") { @cmd = "complete" }
        opts.on("correct", "correct a text") { @cmd = "correct" }
      end

      @parser.parse

      @api_key = ENV["GPT_API_KEY"]? unless @api_key

      begin
        config = ::Config.file(CFG_FNAME.to_s)
        puts "reading config: #{CFG_FNAME}" if verbose
        @api_key = config.as_s?("api_key") unless @api_key
        @lang = config.as_s?("lang") unless @lang
      rescue err : File::NotFoundError
        puts "no config file #{CFG_FNAME}" if @verbose
        # ignore config file if there isn't one
      end
    end

    def usage
      @parser.to_s
    end
  end
end
