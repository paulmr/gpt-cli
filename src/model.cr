require "json"

module GPT
  module Model
    struct Error
      include JSON::Serializable
      property message, code
      def initialize(@message : String, @code : String)
      end
    end
    struct Message
      include JSON::Serializable
      property role : String, content : String, name : String?
      def initialize(@role, @content)
      end
    end
    struct CompletionsRequest
      include JSON::Serializable
      property messages : Array(Message), model : String
      def initialize(@messages, @model)
      end
    end
    struct Usage
      include JSON::Serializable
      property completion_tokens : Int32, prompt_tokens : Int32, total_tokens : Int32
    end
    struct Choice
      include JSON::Serializable
      property finish_reason : String, index : Int32, message : Message
    end
    struct CompletionsResponse
      include JSON::Serializable
      property id : String, model : String, usage : Usage, choices : Array(Choice)
    end
  end
end
