require "colorize"

module GPTCli
  module Commands
    class Complete < Command
      def run(args = ARGV)
        msg = GPT::Model::Message.new(role: "user", content: get_text(args))
        res = @gpt.completions([msg], "gpt-3.5-turbo-1106")
        msg = res.choices[0].message
        @output.puts msg.content
      end
    end
  end
end
