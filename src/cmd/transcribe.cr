module GPTCli
  module Commands
    class Transcribe < Command
      def run(args = ARGV)
        if args.size == 0
          puts "missing file argument"
          puts @cfg.usage
          exit(1)
        end

        puts "Language: #{@cfg.lang}" if @cfg.verbose && @cfg.lang

        begin
          args.each do |fname|
            puts "Processing: #{fname}" if @cfg.verbose
            p = Path.new(fname)
            res = @gpt.transcriptions(File.open(p), p.basename, language: @cfg.lang)
            puts res
          end
        rescue ex
          puts "Error: #{ex}"
          exit(1)
        end
      end
    end
  end
end
