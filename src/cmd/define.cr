module GPTCli
  module Commands
    class Define < PromptCommand
      @language = "french"
      def get_prompt : String
        "Please give me a definition of the following words in " \
        "#{@language}. Give the definition and the grammatical role "\
        "of the word(s). Please reply in #{@language}."
      end
      def initialize(gpt, cfg)
        super(gpt, cfg)
        if (l = cfg.lang)
          @language = l
        end
      end
    end
  end
end
