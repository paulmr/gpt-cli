module GPTCli
  module Commands
    abstract class Command
      def initialize(@gpt : GPT::Client, @cfg : Config, @output : IO = STDOUT)
      end
      def get_text(args) : String
        if args.size > 0
          args.join(" ")
        else
          STDIN.gets_to_end
        end
      end
    end

    abstract class PromptCommand < Command
      # just represents a command that sends a prompt and gets a
      # completion, only the prompt varies
      abstract def get_prompt : String

      def run(args = ARGV)
        prompt = GPT::Model::Message.new(role: "system",
                                         content: get_prompt)
        msgtxt = get_text(args)
        msg = GPT::Model::Message.new(role: "user", content: msgtxt)
        res = @gpt.completions([prompt, msg], "gpt-3.5-turbo-1106")
        msg = res.choices[0].message
        @output.puts msg.content
      end

    end
  end
end
