module GPTCli
  module Commands
    class Correct < PromptCommand
      @language = "french"
      def get_prompt : String
        "You are a teacher in #{@language} and you are going to correct " \
        "the sentence I give you. Please provide a corrected version of " \
        "the sentence with an explanation of the corrections you have " \
        "made. Please only reply to me in #{@language}."
      end
      def initialize(gpt, cfg, io)
        super(gpt, cfg, io)
        if (l = cfg.lang)
          @language = l
        end
      end
    end
  end
end
