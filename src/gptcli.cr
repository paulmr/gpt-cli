require "./config.cr"
require "./gptclient.cr"
require "./cmd/*"
require "config"
require "option_parser"

module GPTCli
  def self.run
    cfg = GPTCli::Config.new

    unless api_key = cfg.api_key
      puts "missing api key"
      puts "config file: " + GPTCli::CFG_FNAME.to_s
      puts cfg.usage
      exit(1)
    end

    gpt = GPT::Client.new(api_key)

    begin
      case cfg.cmd
      when "transcribe"
        GPTCli::Commands::Transcribe.new(gpt, cfg).run
      when "complete"
        GPTCli::Commands::Complete.new(gpt, cfg).run
      when "correct"
        GPTCli::Commands::Correct.new(gpt, cfg).run
      when "define"
        GPTCli::Commands::Define.new(gpt, cfg).run
      else
        puts "unknown command: #{cfg.cmd}"
      end
    rescue e
      puts "Error : #{e}"
    end
  end
end

GPTCli.run
