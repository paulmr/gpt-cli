require "http/client"
require "json"
require "./model.cr"

module GPT
  class Error < Exception
    property error
    def initialize(@error : GPT::Model::Error)
      super("[#{@error.code}] #{@error.message}")
    end
    def initialize(src : String)
      err = GPT::Model::Error.from_json(src, "error")
      initialize(err)
    end
  end

  class Client
    @api_key : String

    def initialize(@api_key, @base_url =  "https://api.openai.com")
      @base_headers = HTTP::Headers.new
      @base_headers.add("Authorization", "Bearer #{@api_key}")
    end

    def transcriptions(file : IO, fname : String, model = "whisper-1", language = nil)
      data = IO::Memory.new
      headers = @base_headers.dup

      HTTP::FormData.build(data) do |form|
        headers.add("Content-Type", form.content_type)
        form.field("model", model)
        form.field("response_format", "text")
        form.field("language", language) if language
        form.file("file", file, HTTP::FormData::FileMetadata.new(fname))
      end
      res = HTTP::Client.post("#{@base_url}/v1/audio/transcriptions", body: data.to_s, headers: headers)
      raise Error.new(res.body) if !res.status.ok?
      res.body
    end

    def completions(msgs : Array(GPT::Model::Message), model : String) : Model::CompletionsResponse
      req = GPT::Model::CompletionsRequest.new(msgs, model).to_json
      hdrs = @base_headers.dup
      hdrs.add("Content-type", "application/json")
      res = HTTP::Client.post("#{@base_url}/v1/chat/completions", body: req, headers: hdrs)
      GPT::Model::CompletionsResponse.from_json(res.body)
    end
  end
end
